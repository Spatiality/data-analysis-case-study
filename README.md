If you want to generate all the plots, pull the dataset.csv into the same directory & run 'python3 code_final.py'

If you want to go through the code step by step, open a notebook ('jupyter notebook') and then open Step_by_step.ipynb (It only runs correctly with the data set, though)
